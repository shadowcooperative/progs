Hints For Newly Commissioned Officers

(Quotation from speech of Major General J. A. Ulio.)

From The Infantry School Mailing List, Volume XXV, February 1943
The Infantry School, Fort Benning Georgia

FOREWORD

The following material is adapted from a pamphlet produced by The Infantry School and distributed to all Officer Candidates upon completion of their training. This subject should be of particular interest to Professors and Assistant Professors of Military Science and Tactics and to members of R.O.T.C. units.

This document is designed to furnish newly commissioned officers with certain information regarding customs, relationships, and procedures which exist in the military service. Many of the matters covered herein are peculiar to the military service and ordinarily are learned only by experience. A lack of knowledge of certain of these may result in unintentional error on the part of anew officer and cause him to start his service at a disadvantage. All newly commissioned officers, therefore, are urged to familiarize themselves with the matters discussed herein and to study the underlying reasons for each of the statements made.

1.     LOYALTY.

All military organization is based upon mutual loyalty between superior and subordinate. You owe to your subordinates your best efforts in planning for their operations and ensuring their welfare. You owe to your superior your intelligent cooperation in such plans as have been made by him for the operations or welfare of the unit. As an example of your duty to your subordinates, you should never look after your own comfort until you are assured that the comfort of all of the members of your unit has been cared for; you should not eat until your men have been fed. Similarly, you should carry out the orders or instructions given you by your superior unless it is manifest that the situation on which those orders were based has so changed that the action ordered will no longer contribute to the carrying out of the general plan. Any variance from orders is your own responsibility.

2.     DISCIPLINE

a.     Justice is the indispensable element of discipline. Any display of favoritism by you will undermine the morale of your organization.

b.     Punishment should be used sparingly. It must be prompt and suited to the offense.

c.     When a soldier has been punished, the entire incident should be forgotten. He should neither be nagged nor continually reminded of his previous dereliction; however, punishments for the same offense should become progressively more severe.

d.     Sarcasm and ridicule are poor instruments of discipline.

e.     Reward, like punishment, should be used sparingly. Praise, when justified, should be given publicly.

3.     COURTESY

a.     You should always address other officers by their titles in the presence of enlisted men.

b.     Noncommissioned officers should invariably be addressed by their titles. They should be required to demand the respect of their subordinates.

4.     COMPLAINTS.

Steps should be taken to ensure that any complaint from an enlisted man be brought to the attention of an officer--usually his company commander. Complaints should be thoroughly and promptly investigated; decisions must be fair.

5.     RELATIONS WITH ENLISTED MEN

a.     Many enlisted men will imitate the officers who command them. It is therefore essential that you set the highest example. This applies not only to your performance of routine duties but to your personal appearance and habits.

b.     You should be particularly meticulous about saluting. You should endeavor never to fail to return a salute from an enlisted man and should seek to return every salute with a better one than you received.

c.     You should cultivate a personal knowledge of all of the enlisted men whom you command, or with whom you come in frequent contact; however, you should scrupulously avoid such intimacy as may tend to result in a loss of respect, or breed familiarity on their part. Your social activities should be separate from enlisted men. You should learn the names of enlisted men as rapidly as possible and use them frequently. You should continually study the capabilities of the men belonging to your unit. You should frequently visit your unit after duty hours. You should encourage your men to present their problems to you and give them such help as you are able to give in solving them.

d.     You should never take advantage of your superior rank to obtain privileges not available to your men. For example, when the enlisted members of your unit are prohibited from smoking, you should not smoke.

6.     USE OF SUBORDINATES

a.     You should endeavor to accomplish as many tasks as possible by issuing instructions to your subordinates. Such a practice trains the subordinates and develops their initiative.

b.     The issuance of orders to subordinates for the accomplishment of a task does not accomplish the task. You retain responsibility for its proper accomplishment until it is done. You must check and supervise continually work that is being done in compliance with your orders.

c.     All orders should be clear and concise. The tone in which an order is given has much to do with the manner of compliance with it. An order should be given confidently and in a tone which indicates no doubt of proper compliance by those to whom it is given.

d.     Never assign tasks to your subordinates in order to conceal your own ignorance or incapacity. If you do not know how a job should be done, you should learn how it should be performed before directing some one else to do it.

7.     CORRECTION OF ENLISTED MEN

a.     Correction of errors or deficiencies on the part of enlisted men should be made promptly.

b.     Whenever possible, the underlying cause of the error or deficiency' should be determined and steps taken to correct this cause.

c.     Corrections should not be made harshly, particularly in the presence of others. Never subject a subordinate to ridicule.

8.     RELATIONS WITH OTHER OFFICERS

a.     Courtesy and a spirit of cooperation are essential elements of the relationship that should exist between officers. Personal dislikes for other officers should be overcome, if possible; if this is not possible, conceal your dislikes.

b.     Controversial subjects such as religion and politics should be avoided.

9.     RELATIONS WITH SUPERIORS

a.     You should be meticulous in rendering courtesies to your superiors.

b.     When an opinion is called for by a superior regarding any matter, the opinion should be honest. Don't try to guess what your superior wants you to say; don't be a "yes man." He wants your opinion.

c.     When a decision has been made, even though it is contrary to any opinion or recommendation which you have expressed or made, you should devote every effort to carrying it out. Never indicate to a subordinate that you are not in accord with the decision of your superior.

10.     MISCELLANEOUS

a.     Avoid debts. Budget your salary and live within it. If you must gamble, do so with officers of approximately the same income as your own.

b.     If you drink, know your capacity and stay well within it. Drink like a gentleman.

c.     Avoid bootlicking. Not only will it hurt you with those with whom you are trying to curry favor, but it will lose for you the respect of your associates.

d.     In your conversation, avoid grumbling, bragging, and continually relating your own personal experiences. Personal stories and incidents do not interest others as they do you.

e.     Avoid gossip. Its results may be serious in many ways.

f.     Do not criticize your superiors to others. Avoid listening to such criticisms by others.

g.     Use military channels for all military correspondence.

h.     Discuss official matters only with those officially concerned. Never discuss them at home.

i.     Learn what regulations and orders to read, and what to study. Volumes of these will come to your attention. You cannot read them all. You should become thoroughly familiar with some. It is up to you to learn as rapidly as possible to distinguish between those which you should scan and those which directly concern you; master the latter. In reading an order which directly concerns you, read it carefully. If it is well written, no words are wasted; hence you cannot afford to skim through it.

j.     Enter into the "off duty" activities of your unit with enthusiasm.

k.     Do not spend your time hunting for a few reasons or regulations that state an order cannot be carried out. It is your job to find at least one way of carrying out any. order you receive.

l.     Develop a sense of promptness; your senior officers will demand it; your men are entitled to it.

m.     Profanity has no place in the vocabulary of an officer.

n.     Arrange your social affairs to conform to scheduled duties. No one cares to do a holiday tour as "officer of the guard" for you.

o.     Make yourself useful when in the presence of your senior officers, but take care to avoid a servile attitude.

p.     Study the principles governing training and the methods of instruction contained in FM 21-5, Military Training, and cultivate the basic qualifications of a successful instructor, not the least of which is a thorough knowledge of one's subject. Lack of preparation on the part of the instructor will ruin any instruction period. Emulate the good instructors you have known; avoid the faults of the poor ones.

q.     "Morale? I'll tell you what morale is. Morale is when a soldier thinks his army is the best in the world; his regiment the best in the army; his company the finest in .the regiment; his squad the best in the company; and that he himself is the best damned soldier in the whole outfit."