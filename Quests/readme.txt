Object 50050 - Quest STEP object
Object 50051 - Quest KEY object
Room 77 - Quest Creation Room
Room 78 - Quest Storage Room

name **questname_step_1**
sdesc questname step# (characters, not sure how long)
ldesc questname step# (characters, not sure how long)
desc space for saving text. (characters, can be very long)
taste (characters- not sure how long)
spare STEP TYPE (INTEGER)
weight delay until next step (INTEGER)
oval0 (INTEGER)
oval1 (INTEGER)
oval2 (INTEGER)
oval3 (INTEGER)
oval4 (INTEGER)
oval5 (INTEGER)
cond (INTEGER)
quality (INTEGER)
cost (INTEGER)
count (INTEGER)

delay x to execute next step
what happens if the player disconnects? We'll check for if the char is delayed or use new linkdead test.
The delay command stops working, how do we let the player continue with the quest?