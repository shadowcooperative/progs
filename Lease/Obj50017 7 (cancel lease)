opprg 50017 7
// Obj50017 7 (cancel lease)
// Created by: Nimrod 1 Mar 2017
// Last modified by: Nimrod 12 Mar 2017 20:42
// Last uploaded by: Nimrod 12 Mar 2017 20:42
set var version Version 0.0.0.6
// Read direction and find target_room vnum
if admin()
  set var debug_output &(3)
else do
  set var debug_output disabled
fi done
set var public_object 50017
set var private_object 50016
set var currency coppers
set var advance_multiple 3
// &(1) should be direction
// grab target_room based on direction used in command
info target_room room -1 exit &(1) vnum
info lease_key room &(target_room) obj &(private_object) oval0
info lease_cost room &(target_room) obj &(private_object) oval1
info lease_time room &(target_room) obj &(private_object) oval2
info lease_end room &(target_room) obj &(private_object) oval3
info lease_began room &(target_room) obj &(private_object) oval4
info lease_obj room &(target_room) obj &(private_object) oval5
info lease_payers room &(target_room) obj &(private_object) cost
info lease_holder room &(target_room) obj &(private_object) taste
info lease_fine room &(target_room) obj &(private_object) spare
info lease_office room -1 obj &(public_object) oval3
info user_name room -1 char -1 name
set var user_name_target &(user_name)
concat user_name_target lessee
info held_key_sn room -1 char -1 inv &(lease_key) oval5
info key_cost room -1 obj &(public_object) oval5
set var timestamp 0
math timestamp timestamp
info lessee_count room &(lease_office) obj &(user_name_target) oval0
info lessee_max room &(lease_office) obj &(user_name_target) oval1
info lessee_fines room &(lease_office) obj &(user_name_target) oval2
info lessee_vnum room &(lease_office) obj &(user_name_target) vnum
if (&(debug_output)=debug)
  vstr target_room: &(target_room) * lease_began: &(lease_began) * required_key &(lease_key)
  vstr held: &(held) * lease_fine: &(lease_fine) * lease_end: &(lease_end)
  vstr key_sn: &(key_sn) * lease_payers: &(lease_payers) * lease_holder: &(lease_holder)
  vstr lease_began: &(lease_began) * user_name: &(user_name) * public_object: &(public_object)
  vstr private_object: &(private_object) * overdue_days: &(overdue_days)* rekey_cost: &(rekey_cost)
  vstr version: &(version) * lessee_count: &(lessee_count) * lessee_max: &(lessee_max)
  vstr lessee_fines: &(lessee_fines) * lessee_vnum: &(lessee_vnum)
  vbr -
fi done
if (&(target_room)=0)
  vstr Syntax: #6CANCEL LEASE <direction>#0
  vbr -
  halt -
fi done
if oexist(&(private_object),&(target_room))
  // vstr The private object exists in &(target_room), continue.
else do
  vstr There is no lease in that direction.
  vbr -
  halt -
fi done
if (&(user_name)=&(lease_holder))
  // vstr It's the lease holder, let them pass.
else do
  // Keyholders cannot request to change the locks, only the lease_holder
  vstr Trying to cancel someone elses lease? Tsk, tsk, tsk.
  vbr -
  halt -
fi done
if (&(lease_end)<&(timestamp))
  vstr You are behind on lease payments.  Please make a payment on your lease before you cancel.
  vstr #6PAY LEASE <dir> <amount>#0
  vbr -
  halt -
fi done
// Check to make sure they really want to cancel the lease, use timestamp as the verification.
if (&(2)=&(timestamp))
  // vstr The timestamp matches.
else do
  vbr -
  vstr If you really wish to cancel your lease, enter the command thus: 
  vstr #6CANCEL LEASE &(1) &(timestamp)#0
  vbr -
  halt -
fi done
// Everything's okay, let's cancel their lease.
setval room &(target_room) obj &(private_object) oval3 0
setval room &(target_room) obj &(private_object) oval4 0
setval room &(target_room) obj &(private_object) taste Empty
setval room &(target_room) obj &(private_object) spare 0
math key_cost divide 2
math key_cost int
vbr -
vstr You have cancelled your lease!
vstr You may turn your key back in for a &(key_cost) copper credit.
atwrite !leases "&(user_name) cancelled &(target_room) lease" &(user_name) just cancelled the lease at &(target_room).
if (&(lessee_count)>0)
  math lessee_count sub 1
  setval room &(lease_office) obj &(user_name) oval0 &(lessee_count)
fi done
if (&(lease_fine)>0)
  vstr There is a fine of &(lease_fine) &(currency) that is being 
  vstr transferred to your account at the office. You will not be
  vstr allowed to lease a new unit until these fines are paid.
  vbr -
  vstr Syntax to pay fine: #6PAY FINE &(1) &(lease_fine)#0
  vbr -
  math lessee_fines add &(lease_fine)
  setval room &(lease_office) obj &(user_name_target) oval2 &(lessee_fines)
fi done
@
